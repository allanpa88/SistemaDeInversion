﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaDeInversion
{
    class Program {
        static void Main(string[] args) {

            int opcion = 0; 
            while (opcion  != 3) {
                opcion = DisplayMenu();

                if ( opcion == 1 ) {
                    VistaCaracter x = new VistaCaracter();
                } else if ( opcion == 2 ) {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new VistaGUI());
                }
                
                Console.ReadKey();
            }

        }

        static public int DisplayMenu() {
            Console.WriteLine("Bienvenido! Escoja el tipo de interfaz que desea utilizar");
            Console.WriteLine();
            Console.WriteLine("1. Modo caracter.");
            Console.WriteLine("2. GUI - Interfaz gráfica");
            Console.WriteLine("3. Exit");
            Console.WriteLine();
            Console.WriteLine("Seleccione una opción para escoger el tipo de interfaz deseado:");
            var result = Console.ReadLine();
            return Convert.ToInt32(result);
        }
    }
}
